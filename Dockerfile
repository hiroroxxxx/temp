FROM maven:3.6.0-jdk-8-slim AS build-stage
COPY . /project
WORKDIR /project/
RUN mvn clean install 
  
FROM ibmcom/websphere-liberty:kernel-java8-ibmjava-ubi
COPY --chown=1001:0 --from=build-stage /project/src/main/liberty/config/server.xml /config/server.xml
COPY --chown=1001:0 --from=build-stage /project/src/main/liberty/config/server.env /config/server.env
COPY --chown=1001:0 --from=build-stage /project/src/main/liberty/config/jvm.options /config/jvm.options
COPY --chown=1001:0 --from=build-stage /project/target/acmeair-mainservice-java-3.3.war /config/apps/
RUN configure.sh
